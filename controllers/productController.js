const Product = require("../models/Product");
const User = require("../models/User");

//Create Product function
module.exports.addProduct = (reqBody, data) => {
	if (data.isAdmin === true) {
		let newProduct = new Product ({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});

		return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		});
	} else {
		return false
	};
};

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

//Get all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	});
};

//Retreive a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

//Updating product details
module.exports.updateProduct = (reqParams, reqBody, data) => {
	if (data.isAdmin === true) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			};
		});
	} else {
		return false
	};
};

//Archiving a product
module.exports.archiveProduct = (reqParams, reqBody, data) => {
	if (data.isAdmin === true) {
		let archivedProduct = {
			isActive: reqBody.isActive
		};
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			};
		});
	} else {
		return false
	};
};