const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User registration

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return true
		} else {
			return false
		}
	});
};

//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				console.log(result)
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	});
};

//Create order function
module.exports.createOrder = async (data) => {
	if (data.isAdmin === true) {
		return false
	} else {
		let updateUser = await User.findById(data.userId).then(user => {
			user.orders.push({
				products: {
					productName: data.name
				},
				totalAmount: data.totalAmount
			});
			return user.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});
		});
		let updateProduct = await Product.findById(data.productId).then(product => {
			product.orders.push({orders: data.userId});
			return product.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});
		});
		if (updateUser && updateProduct) {
			return true
		} else {
			return false
		};
	};
};

//Get user profile
module.exports.getProfile = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	});
};
