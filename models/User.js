const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is requried"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			products: [
				{
					productName: {
						type: String,
						required: [true, "Product name is required"]
					}
				}
			],
			totalAmount: {
				type: Number,
				required: [true, "Amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);