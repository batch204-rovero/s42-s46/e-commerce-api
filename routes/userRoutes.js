const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: req.body.userId,
		name: req.body.name,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

//Retreive user details
router.get("/details", (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile(userData).then(resultFromController => res.send(resultFromController)); 
});

module.exports = router;