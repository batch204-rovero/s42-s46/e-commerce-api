const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth.js");

//Route for creating products
router.post("/", (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send("User is not an admin, unable to create product");
	}
});

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});
//Route for retreiving all active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//Retreiving a specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//Updating product details
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		return false
	};
});

//Archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === true) {
		productController.archiveProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	};
});

module.exports = router;